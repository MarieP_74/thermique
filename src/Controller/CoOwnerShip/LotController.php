<?php

namespace App\Controller\CoOwnerShip;

use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Lot;
use App\Form\LotType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route({
 *  "en": "/admin/project/{referenceId}",
 *  "fr": "/admin/projet/{referenceId}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"referenceId" = "id"}})
 */
class LotController extends AbstractController
{
    /**
     * @Route("/lots", name="lots_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership): Response
    {
        $repository = $this->getDoctrine()->getRepository(Lot::class);
        $lots = $paginator->paginate(
          $repository->findLotsByCoOwnership($coOwnership),
          $request->query->getInt('pageLot', 1),
          15,
            ['pageParameterName' => 'pageLot', 'sortDirectionParameterName' => 'dirLot']
        );

        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'lots' => $lots,
            'object' => 'lot',
            'pagination' => $lots
        ]);
    }

    /**
     * @Route({
     *  "en": "/lot/add",
     *  "fr": "/lot/ajouter"
     * }, name="lot_new", methods={"GET","POST"})
     */
    public function new(Request $request, CoOwnership $coOwnership): Response
    {
        $lot = new Lot();
        $form = $this->createForm(LotType::class, $lot);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lot);
            $entityManager->flush();

            return $this->redirectToRoute('lots_index', ['referenceId' => $coOwnership->getId()]);
        }

        return $this->render('object/new.html.twig', [
            'reference' => $coOwnership,
            'entity' => $lot,
            'form' => $form->createView(),
            'object' => 'lot'
        ]);
    }

    /**
     * @Route("/lot/{id}", name="lot_show", methods={"GET"})
     */
    public function show(Lot $lot, CoOwnership $coOwnership): Response
    {
        return $this->render('object/view.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'entity' => $lot,
            'object' => 'lot'
        ]);
    }

    /**
     * @Route({
     *  "en": "/lot/{id}/edit",
     *  "fr": "/lot/{id}/modifier"
     * }, name="lot_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Lot $lot, CoOwnership $coOwnership): Response
    {
        $form = $this->createForm(LotType::class, $lot);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lots_index', [
                'referenceId' => $coOwnership->getId(),
            ]);
        }

        return $this->render('object/edit.html.twig', [
            'reference' => $coOwnership,
            'entity' => $lot,
            'form' => $form->createView(),
            'object' => 'lot'
        ]);
    }

    /**
     * @Route({
     *  "en": "/lot/{id}/delete",
     *  "fr": "/lot/{id}/supprimer"
     * }, name="lot_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Lot $lot, CoOwnership $coOwnership): Response
    {
        if ($this->isCsrfTokenValid('delete'.$lot->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lot);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lots_index', ['referenceId' => $coOwnership->getId()]);
    }
}
