<?php

namespace App\Controller\Grant;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\Scenario\Scenario;
use App\Entity\Grant\Grant;
use App\Form\Grant\GrantType;
use App\Form\Grant\AddGrantType;
use App\Form\Grant\TestGrantType;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\GenerateSubsidies;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * @Route({
 *  "en": "/admin/project/{referenceId}",
 *  "fr": "/admin/projet/{referenceId}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"referenceId" = "id"}})
 */
class GrantController extends AbstractController
{
    private $appKernel;
    private $generateSubsidies;

    public function __construct(KernelInterface $appKernel, GenerateSubsidies $generateSubsidies)
    {
        $this->appKernel = $appKernel;
        $this->generateSubsidies = $generateSubsidies;
    }

    /**
     * @Route({
     *  "en": "/grants",
     *  "fr": "/grants"
     * }, name="grants_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership, int $maxItemsPerPage = 15): Response
    {
        $repository = $this->getDoctrine()->getRepository(Grant::class);
        $grants = $paginator->paginate(
            $coOwnership->getGrants(),
            $request->query->getInt('page', 1),
            $maxItemsPerPage
        );

        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'grants' => $grants,
            'object' => 'grant'
        ]);
    }
    
  /**
   * @Route({
   *  "en": "/grant/add",
   *  "fr": "/grant/ajouter"
   * }, name="grant_new")
   */
  public function addGrant(Request $request, CoOwnership $coOwnership)
  {
      $grant = null;
      $form = $this->createForm(AddGrantType::class);

      $form->handleRequest($request);


      if($form->isSubmitted() && $form->isValid()) {
          $param = $form->get('grant')->getData();
          $grant = new Grant();
          $grant->setName($param->getName());
          $grant->setSetting($param->getSetting());
          $grant->setPosition($form->get('position')->getData());
          $grant->setPopulation($param->getPopulation());
          $grant->setCoOwnership($coOwnership);
          $em = $this->getDoctrine()->getManager();
          $em->persist($grant);

          $em->flush();

          return $this->redirectToRoute('grant_edit', ['referenceId' => $coOwnership->getId(), 'id' => $grant->getId()]);
      }

      return $this->render('object/new.html.twig', [
          'reference' => $coOwnership,
        'form' => $form->createView(),
          'object' => 'grant',
          'entity' => $grant
      ]);
  }

    /**
     * @Route({
     *  "en": "/grant/{id}",
     *  "fr": "/grants/{id}"
     * }, name="grant_show", methods={"GET"})
     */
    public function show(Grant $grant, CoOwnership $coOwnership): Response
    {
        return $this->render('object/view.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'entity' => $grant,
            'object' => 'grant'
        ]);
    }

    /**
     * @Route({
     *  "en": "/grant/{id}/edit",
     *  "fr": "/grants/{id}/modifier"
     * }, name="grant_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Grant $grant, CoOwnership $coOwnership): Response
    {
        $form = $this->createForm(GrantType::class, $grant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('grants_index', [
                'referenceId' => $coOwnership->getId(),
            ]);
        }

        return $this->render('object/edit.html.twig', [
            'reference' => $coOwnership,
            'entity' => $grant,
            'form' => $form->createView(),
            'object' => 'grant'
        ]);
    }

    /**
     * @Route({
     *  "en": "/grant/{id}/delete",
     *  "fr": "/grants/{id}/supprimer"
     * }, name="grant_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Grant $grant, CoOwnership $coOwnership): Response
    {
        if ($this->isCsrfTokenValid('delete'.$grant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($grant);
            $entityManager->flush();
        }

        return $this->redirectToRoute('grants_index', ['referenceId' => $coOwnership->getId()]);
    }

    protected function testOwner(Grant $grant, Owner $owner, CoOwnership $coOwnership)
    {
        $results = array();
        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
//        $jsonSchemaPath = $this->appKernel->getProjectDir(). '/assets/';
        foreach ($scenarios as $scenario) {
            $results[] = $this->generateSubsidies->generateSubsidies($owner, $scenario, $grant);
        }
        $test = [];
        $test['owner'] = $owner;
        $test['grant'] = $grant;
        $test['results'] = $results;
        return $test;
    }

    /**
     * @Route({
     *  "en": "/grant/{id}/test/{owner_id}",
     *  "fr": "/grant/{id}/test/{owner_id}"
     * }, name="grant_test", defaults={"owner_id" = null})
     * @ParamConverter("owner", class=Owner::class, options={"mapping": {"owner_id": "id"}})
     */
    public function test(Request $request, Grant $grant, Owner $owner = null, CoOwnership $coOwnership)
    {
        if($owner == null) {
            $form = $this->createForm(TestGrantType::class, null, ['coOwnership' => $coOwnership]);

            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {
                $owner = $form->get('owner')->getData();

                return $this->redirectToRoute('grant_test', ['referenceId' => $coOwnership->getId(), 'id' => $grant->getId(), 'owner_id' => $owner->getId()]);
            }

            return $this->render('object/new.html.twig', [
                'reference' => $coOwnership,
                'form' => $form->createView(),
                'object' => 'grant',
                'entity' => $grant
            ]);
        }

        $test = $this->testOwner($grant, $owner, $coOwnership);

        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
        return $this->render('coOwnership/grant/test.html.twig', [
            'scenarios' => $scenarios,
            'test' => $test,
            'coOwnership' => $coOwnership
        ]);

    }

    /**
     * @Route({
     *  "en": "/grant/test_all",
     *  "fr": "/grant/test_all"
     * }, name="grant_test_all")
     */
    public function testAll(Request $request, CoOwnership $coOwnership)
    {
        $repository = $this->getDoctrine()->getRepository(Owner::class);
        $owners = $repository->findOwnerByCoOwnership($coOwnership, true)->getQuery()->getResult();

        $tests = [];
        foreach($owners as $owner) {
            foreach($coOwnership->getGrants() as $grant) {
                $tests[] = $this->testOwner($grant, $owner, $coOwnership);
            }
//            break;
        }

        $repository = $this->getDoctrine()->getRepository(Scenario::class);
        $scenarios = $repository->findScenariosByCoOwnership($coOwnership)->getResult();
        return $this->render('coOwnership/grant/test_all.html.twig', [
            'scenarios' => $scenarios,
            'tests' => $tests,
            'coOwnership' => $coOwnership
        ]);

    }

}
