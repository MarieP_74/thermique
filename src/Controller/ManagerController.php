<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ManagerController extends AbstractController
{
    /**
     * @Route("/manager/", name="manager")
     */
    public function index()
    {
        return $this->render('manager/manager.html.twig', [
            'controller_name' => 'ManagerController',
        ]);
    }



}
