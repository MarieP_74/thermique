<?php

namespace App\Controller\Scenario;

use App\Entity\Scenario\Work;
use App\Form\WorkType;
use App\Entity\CoOwnerShip\CoOwnership;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route({
 *  "en": "/admin/project/{referenceId}",
 *  "fr": "/admin/projet/{referenceId}"
 * })
 * @ParamConverter("coOwnership", class=CoOwnership::class, options={"mapping": {"referenceId" = "id"}})
 */
class WorkController extends AbstractController
{
    /**
     * @Route({
     *  "en": "/works",
     *  "fr": "/travaux"
     * }, name="works_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, CoOwnership $coOwnership, int $maxItemsPerPage = 15): Response
    {
        $repository = $this->getDoctrine()->getRepository(Work::class);
        $works = $paginator->paginate(
          $repository->findWorksByCoOwnership($coOwnership),
          $request->query->getInt('page', 1),
          $maxItemsPerPage
        );

        return $this->render('object/index.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'works' => $works,
            'object' => 'work'
        ]);
    }

    /**
     * @Route({
     *  "en": "/work/add",
     *  "fr": "/travaux/ajouter"
     * }, name="work_new", methods={"GET","POST"})
     */
    public function new(Request $request, CoOwnership $coOwnership)
  {
      $repo = $this->getDoctrine()->getRepository(CoOwnership::class);
      $work = new Work();
      $form = $this->createForm(WorkType::class, $work, [
        'coOwnership' => $coOwnership,
      ]);

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($work);

          foreach($work->getWorkHasBuildings() as $workHasBuilding) {
            $workHasBuilding->setWork($work);
          }

          $em->flush();

          return $this->redirectToRoute('work_new', [
            'referenceId' => $coOwnership->getId()
          ]);
      }

      return $this->render('coOwnership/work/new.html.twig', [
        'reference' => $coOwnership,
        'form' => $form->createView(),
          'object' => 'work'
      ]);
  }

    /**
     * @Route({
     *  "en": "/work/{id}",
     *  "fr": "/travaux/{id}"
     * }, name="work_show", methods={"GET"})
     */
    public function show(Work $work, CoOwnership $coOwnership): Response
    {
        return $this->render('object/view.html.twig', [
            'reference' => $coOwnership,
            'origin' => 'coOwnership',
            'entity' => $work,
            'object' => 'work'
        ]);
    }

    /**
     * @Route({
     *  "en": "/work/{id}/edit",
     *  "fr": "/travaux/{id}/modifier"
     * }, name="work_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Work $work, CoOwnership $coOwnership): Response
    {
        $form = $this->createForm(WorkType::class, $work, [
          'coOwnership' => $coOwnership,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach($work->getWorkHasBuildings() as $workHasBuilding) {
                $workHasBuilding->setWork($work);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('works_index', [
                'referenceId' => $coOwnership->getId(),
            ]);
        }

        return $this->render('coOwnership/work/new.html.twig', [
            'reference' => $coOwnership,
            'entity' => $work,
            'form' => $form->createView(),
            'object' => 'work'
        ]);
    }

    /**
     * @Route({
     *  "en": "/work/{id}/delete",
     *  "fr": "/travaux/{id}/supprimer"
     * }, name="work_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Work $work, CoOwnership $coOwnership): Response
    {
        if ($this->isCsrfTokenValid('delete'.$work->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($work);
            $entityManager->flush();
        }

        return $this->redirectToRoute('works_index', ['referenceId' => $coOwnership->getId()]);
    }
}
