<?php

namespace App\Controller\Scenario;


use App\Entity\Scenario\WorkHasBuilding;
use App\Entity\Scenario\Work;
use App\Form\WorkHasBuildingType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route({
 *  "en": "/admin/work/{referenceId}",
 *  "fr": "/admin/work/{referenceId}"
 * })
 * @ParamConverter("work", class=Work::class, options={"mapping": {"referenceId" = "id"}})
 */
class WorkHasBuildingController extends AbstractController
{
    /**
     * @Route("/workHasBuildings", name="workHasBuildings_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, Work $work): Response
    {
        $repository = $this->getDoctrine()->getRepository(WorkHasBuilding::class);
        $workHasBuildings = $paginator->paginate(
            $repository->findBy(array('work' => $work)),
            $request->query->getInt('page', 1),
            15
        );

        return $this->render('object/index.html.twig', [
            'reference' => $work,
            'origin' => 'coOwnership',
            'workHasBuildings' => $workHasBuildings,
            'object' => 'workHasBuilding'
        ]);
    }

    /**
     * @Route({
     *  "en": "/workHasBuilding/add",
     *  "fr": "/workHasBuilding/ajouter"
     * }, name="workHasBuilding_new", methods={"GET","POST"})
     */
    public function newAction(Request $request, Work $work): Response
    {
        $workHasBuilding = new WorkHasBuilding();
        $workHasBuilding->setWork($work);
        $form = $this->createForm(WorkHasBuildingType::class, $workHasBuilding, [
            'coOwnership' => $work->getCoOwnership(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($workHasBuilding);
            $entityManager->flush();

            return $this->redirectToRoute('workHasBuildings_index', ['referenceId' => $work->getId()]);
        }

        return $this->render('object/new.html.twig', [
            'reference' => $work,
            'entity' => $workHasBuilding,
            'form' => $form->createView(),
            'object' => 'workHasBuilding'
        ]);
    }

    /**
     * @Route("/workHasBuilding/{id}", name="workHasBuilding_show", methods={"GET"})
     */
    public function show(WorkHasBuilding $workHasBuilding, Work $work): Response
    {
        return $this->render('object/view.html.twig', [
            'reference' => $work,
            'origin' => 'coOwnership',
            'entity' => $workHasBuilding,
            'object' => 'workHasBuilding'
        ]);
    }

    /**
     * @Route({
     *  "en": "/workHasBuilding/{id}/edit",
     *  "fr": "/workHasBuilding/{id}/modifier"
     * }, name="workHasBuilding_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, WorkHasBuilding $workHasBuilding, Work $work): Response
    {
        $form = $this->createForm(WorkHasBuildingType::class, $workHasBuilding,  [
            'coOwnership' => $work->getCoOwnership(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('workHasBuildings_index', [
                'referenceId' => $work->getId(),
            ]);
        }

        return $this->render('coOwnership/workHasBuilding/edit.html.twig', [
            'work' => $work,
            'entity' => $workHasBuilding,
            'form' => $form->createView(),
            'object' => 'workHasBuilding'
        ]);
    }

    /**
     * @Route({
     *  "en": "/workHasBuilding/{id}/delete",
     *  "fr": "/workHasBuilding/{id}/supprimer"
     * }, name="workHasBuilding_delete", methods={"DELETE"})
     */
    public function delete(Request $request, WorkHasBuilding $workHasBuilding, Work $work): Response
    {
        if ($this->isCsrfTokenValid('delete'.$workHasBuilding->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($workHasBuilding);
            $entityManager->flush();
        }

        return $this->redirectToRoute('workHasBuildings_index', ['referenceId' => $work->getId()]);
    }
}
