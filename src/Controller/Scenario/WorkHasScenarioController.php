<?php

namespace App\Controller\Scenario;


use App\Entity\Scenario\WorkHasScenario;
use App\Entity\Scenario\Work;
use App\Form\WorkHasScenarioType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route({
 *  "en": "/admin/work/{referenceId}",
 *  "fr": "/admin/work/{referenceId}"
 * })
 * @ParamConverter("work", class=Work::class, options={"mapping": {"referenceId" = "id"}})
 */
class WorkHasScenarioController extends AbstractController
{
    /**
     * @Route("/workHasScenarios", name="workHasScenarios_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator, Request $request, Work $work): Response
    {
        $repository = $this->getDoctrine()->getRepository(WorkHasScenario::class);
        $workHasScenarios = $paginator->paginate(
          $repository->findBy(array('work' => $work)),
          $request->query->getInt('page', 1),
          15
        );

        return $this->render('object/index.html.twig', [
            'reference' => $work,
            'origin' => 'coOwnership',
            'workHasScenarios' => $workHasScenarios,
            'object' => 'workHasScenario'
        ]);
    }

    /**
     * @Route({
     *  "en": "/workHasScenario/add",
     *  "fr": "/workHasScenario/ajouter"
     * }, name="workHasScenario_new", methods={"GET","POST"})
     */
    public function newAction(Request $request, Work $work): Response
    {
        $workHasScenario = new WorkHasScenario();
        $workHasScenario->setWork($work);
        $form = $this->createForm(WorkHasScenarioType::class, $workHasScenario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($workHasScenario);
            $entityManager->flush();

            return $this->redirectToRoute('workHasScenarios_index', ['referenceId' => $referenceId]);
        }

        return $this->render('object/new.html.twig', [
            'reference' => $work,
            'entity' => $workHasScenario,
            'form' => $form->createView(),
            'object' => 'workHasScenario'
        ]);
    }

    /**
     * @Route("/workHasScenario/{id}", name="workHasScenario_show", methods={"GET"})
     */
    public function show(WorkHasScenario $workHasScenario, Work $work): Response
    {
        return $this->render('coOwnership/workHasScenario/show.html.twig', [
            'work' => $work,
            'entity' => $workHasScenario,
            'object' => 'workHasScenario'
        ]);
    }

    /**
     * @Route({
     *  "en": "/workHasScenario/{id}/edit",
     *  "fr": "/workHasScenario/{id}/modifier"
     * }, name="workHasScenario_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, WorkHasScenario $workHasScenario, Work $work): Response
    {
        $form = $this->createForm(WorkHasScenarioType::class, $workHasScenario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('workHasScenarios_index', [
                'referenceId' => $work->getId(),
            ]);
        }

        return $this->render('coOwnership/workHasScenario/edit.html.twig', [
            'work' => $work,
            'entity' => $workHasScenario,
            'form' => $form->createView(),
            'object' => 'workHasScenario'
        ]);
    }

    /**
     * @Route({
     *  "en": "/workHasScenario/{id}/delete",
     *  "fr": "/workHasScenario/{id}/supprimer"
     * }, name="workHasScenario_delete", methods={"DELETE"})
     */
    public function delete(Request $request, WorkHasScenario $workHasScenario, Work $work): Response
    {
        if ($this->isCsrfTokenValid('delete'.$workHasScenario->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($workHasScenario);
            $entityManager->flush();
        }

        return $this->redirectToRoute('workHasScenarios_index', ['referenceId' => $work->getId()]);
    }
}
