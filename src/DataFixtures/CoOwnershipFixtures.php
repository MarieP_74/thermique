<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\CoOwnerShip\CoOwnership;
use App\Entity\Category;
use Faker;

class CoOwnershipFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 1; $i <= 70; $i++) {
            $coownership = new CoOwnership();
            $coownership->setBusinessNumber($i)
                        ->setResidenceName($faker->name)
                        ->setResidenceType($faker->word)
                        ->setAddress($faker->streetName)
                        ->setPostalCode(2222)
                        ->setCity($faker->city)
                        ->setDepartment($faker->numberBetween($min = 1, $max = 95))
                        ->setClimaticArea($faker->word)
                        ->setProductionType($faker->word)
                        ->setConstructionDate(new \DateTime())
                        ->setCoOwnershipFragile(true)
                        ->setWorksFund($faker->randomDigitNotNull)
                        ->setSurveyOpeningDate(new \DateTime())
                        ->setSurveyClosingDate(new \DateTime())
                        ->setNumberOfHousings($faker->randomDigitNotNull)
                        ->setLivingSpace($faker->randomDigitNotNull)
                        ->setNumberOfShops($faker->randomDigit)
                        ->setNumberOfOffices($faker->randomDigit)
                        ->setLivingSpaceAverage($faker->randomDigitNotNull)
                        ->setProjectFees($faker->numberBetween($min = 1000, $max = 9000))
                        ->setAuditInitialDate(new \DateTime())
                        ->setAuditDoneByOffice(false)
                        ->setEnergyInitialTag($faker->word)
                        ->setExonerationPropertyTax($faker->word)
                        ->setTantiemesTotal($faker->randomNumber($nbDigits = NULL, $strict = false))
                        ->setFile('oui');
                        
        $manager->persist($coownership);
        }
        $manager->flush();
    }
}
