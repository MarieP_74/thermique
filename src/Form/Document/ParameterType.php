<?php

namespace App\Form\Document;

use App\Entity\Document\DocumentCategory;
use PhpParser\Comment\Doc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Repository\Document\ParameterRepository;

class ParameterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,  array(
                "label"=>"Grant.name"))
            ->add('setting', ChoiceType::class, [
                'choices' => DocumentCategory::$categoriesChoices,
                'attr' => [
                    'placeholder' => 'Choisissez un type'],
                'label' => 'Type de document',
                'required' => false,
                'empty_data' =>"0",
            ])
            ->add('population', ChoiceType::class, [
                'choices' => DocumentCategory::$populationsChoices,
                'attr' => [
                    'placeholder' => 'Choisissez un type de destinataires'],
                'label' => 'Destinataires',
                'required' => false,
                'empty_data' =>"0",
            ])
            ->add('childCategory', EntityType::class, [
                'class' => DocumentCategory::class,
                'required' => false,
                'query_builder' => function(ParameterRepository $wr) use($options) {
                    return $wr->queryDocuments();
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DocumentCategory::class,
        ]);
    }
}
