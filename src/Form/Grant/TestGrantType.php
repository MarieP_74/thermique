<?php

namespace App\Form\Grant;

use App\Entity\Grant\Grant;
use App\Entity\CoOwnerShip\Owner;
use App\Entity\CoOwnerShip\CoOwnership;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Repository\OwnerRepository;

class TestGrantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('owner', EntityType::class, [
                'class' => Owner::class,
                'query_builder' => function(OwnerRepository $wr) use($options) {
                    return $wr->findOwnerByCoOwnership($options['coOwnership']);
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['coOwnership']);
        $resolver->setDefaults([
            'data_class' => null,
                'coOwnership' => null

        ]);
    }
}
