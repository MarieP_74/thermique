<?php

namespace App\Form;

use App\Entity\CoOwnerShip\Owner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OwnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender', TextType::class, [
                'required' => false
            ])
            ->add('firstname', TextType::class, [
                'required' => false
            ])
            ->add('lastname', TextType::class, [
                'required' => false
            ])
            ->add('company', TextType::class, [
                'required' => false,
                'empty_data' =>"",
            ])
            ->add('postalCode', TextType::class, [
                'required' => false
            ])
            ->add('address1', TextType::class, [
                'required' => false,
                'empty_data' =>"",
            ])
            ->add('address2', TextType::class, [
                'required' => false,
                'empty_data' =>"",
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'empty_data' =>"",
            ])
            ->add('country', TextType::class, [
                'required' => false,
                'empty_data' =>"",
            ])
            ->add('phone1', TextType::class, [
                'required' => false,
                'empty_data' =>"",
            ])
            ->add('phone2', TextType::class, [
                'required' => false,
                'empty_data' =>"",
            ])
            ->add('mail', TextType::class, [
                'required' => false,
                'empty_data' =>"",
            ])
            ->add('manualForm', CheckboxType::class, [
                'required' => false
            ])
            ->add('personalData', CheckboxType::class, [
                'required' => false
            ])
            ->add('paperCopy', CheckboxType::class, [
                'required' => false
            ])
            ->add('childrenNumber', IntegerType::class, [
                'required' => false,
                'empty_data' =>"0",
            ])
            ->add('maritalStatus', ChoiceType::class, [
                'choices' => Owner::$maritalsChoices,
                'attr' => [
                    'placeholder' => 'Choisissez une option'],
                'label' => 'Statut marital',
                'required' => false,
                'empty_data' =>"0",
            ])
            ->add('retired', CheckboxType::class, [
                'required' => false
            ])
            ->add('housingSituation', ChoiceType::class, [
                'choices' => Owner::$housingsChoices,
                'attr' => [
                    'placeholder' => 'Choisissez une option'
                ],
                'label' => 'Situation',
                'required' => false
            ] )
            ->add('taxRevenueOne', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
            ])
            ->add('taxRevenueTwo', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
            ])
            ->add('gotAnah', CheckboxType::class, [
                'required' => false
            ])
            ->add('subAnahAmount', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
            ])
            ->add('gotTaxCredit', CheckboxType::class, [
                'required' => false
            ])
            ->add('taxCreditAmount', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
            ])
            ->add('gotEcoCredit', CheckboxType::class, [
                'required' => false
            ])
            ->add('ecoCreditAmount', NumberType::class, [
                'required' => false,
                'empty_data' =>"0",
            ])
            ->add('gotPTZ', CheckboxType::class, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Owner::class,
        ]);
    }
}
