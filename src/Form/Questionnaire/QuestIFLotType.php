<?php

namespace App\Form\Questionnaire;

use App\Entity\CoOwnerShip\Lot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class QuestIFLotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('staircase',TextType::class, [
                'required' => false,
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'ex : 3B'
                ],
                'label' => 'Montée'
            ])
            ->add('floor', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'ex: 4'
                ],
                'label' => 'Etage'
            ])
            ->add('door', TextType::class, [
                'required' => false,
                'empty_data' => '',
                'attr' => [
                    'placeholder' => 'ex: 14'
                ],
                'label' => 'Porte'
            ])
            ->add('apartmentType', TextType::class, [
                'attr' => [
                    'placeholder' => 'ex: T4'
                ],
                'label' => 'Type d\'appartement'
            ])
            ->add('roomNumber', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'ex: 3',
                ],
                'label' => 'Nombre de pièces après travaux'
            ])
            ->add('roomArea', IntegerType::class, [
                'required' => false,
                'empty_data' =>"0",
                'attr' => [
                    'placeholder' => 'ex: 67',
                ],
                'label' => 'Surface habitable après travaux (m²)'
            ])
//            ->add('yearEntry', IntegerType::class, [
//                'attr' => [
//                    'placeholder' => 'ex: 1998',
//                ],
//                'label' => 'Année d\'entrée'
//            ])
            ->add('mainResidence', CheckboxType::class, [
                'label' => 'Cochez cette case si c\'est votre résidence principale',
                'required' => false,
//                'attr' => [
//                    'class' => 'switch_1'
//                ]
            ])
            ->add('apartmentNumber', IntegerType::class, [
                'required' => true,
                'empty_data' => '1',
                'attr' => [
                    'placeholder' => 'ex: 1',
                ],
                'label' => 'Nombre de logements vous appartenant dans la résidence'
            ])
            ->add('submit', SubmitType::class, array('label' => 'Enregistrer et terminer'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lot::class,
            'validation_groups' => ['Default']
        ]);
    }
}