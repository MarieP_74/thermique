<?php

namespace App\Repository;

use App\Entity\Scenario\WorkHasScenario;
use App\Entity\Scenario\Scenario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WorkHasScenario|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkHasScenario|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkHasScenario[]    findAll()
 * @method WorkHasScenario[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkHasScenarioRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WorkHasScenario::class);
    }

    // /**
    //  * @return WorkHasScenario[] Returns an array of WorkHasScenario objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WorkHasScenario
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findWorkHasScenariosByScenario(Scenario $scenario) {
        $qb = $this->createQueryBuilder('whs')
            ->andWhere('whs.scenario = :scenario')
            ->setParameter('scenario', $scenario)
            ->getQuery();

        return $qb;
    }
}
