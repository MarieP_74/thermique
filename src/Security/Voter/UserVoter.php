<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\User\User;
use App\Entity\CoOwnerShip\Owner;
use Symfony\Component\Security\Core\Security;

class UserVoter extends Voter
{
    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['POST_EDIT', 'POST_VIEW'])
            && $subject instanceof User;
    }
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        if($this->security->isGranted('ROLE_ADMIN'))
            return true;

        if($this->security->isGranted('ROLE_USER')) {
            switch ($attribute) {
                case 'POST_VIEW':
                case 'POST_EDIT':
                // dump($subject);dump($user);
                // die('ici');
                if ($subject->getId() == $user->getId()) {
                    return true;
                }
                break;
            }
        }

        return false;
        // ... (check conditions and return true to grant permission) ...
    }
}
